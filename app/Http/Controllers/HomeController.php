<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use PDF;

class HomeController extends Controller
{
    public function index() 
    {
        return view('pdf');
    }

    public function save_pdf() 
    {
         $pdf = PDF::loadView('pdf');
        return $pdf->download('surat.pdf');
    }
}
